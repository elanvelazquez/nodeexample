//Packages
var express = require('express');
var router = express.Router();
var User = require('../models/user').User;
var jwt = require('jsonwebtoken');
var app = express();
var config = require('../config')

//import private routes 
router.use('/api/company', require('./company'));
router.use('/api/user', require('./user'));
router.use('/api/location', require('./location'));
 
//there are the public routes.
router.get('/',function(req,res){
    res.json({});
    });
//Signup for new users
router.post('/signup',function(req,res){
    console.log('add user');
        var data = {
            name: req.body.name,
            password: req.body.password,
            role: 'USER'
        };
        var user = new User(data);
        user.save(function (err,obj) {
            if (err) {
                res.status(403).send({
                    error: true,
                    message: 'Error adding user'
                });
            } else {
                console.log('save data:' + data);
                 // if mongoose save the data, then generate a jwt
                var token = jwt.sign(user,String( app.get('superSecret')), {
                    expiresIn: 1440 // tiempo de expiración, checar documentacion
                });
                res.json({
                    error:'false',
                    token:token
                });

                res.end();
            }
            res.end(); 
    });
});
//SignIn for old users
router.post('/signin', function(req,res){
    console.log('Inicio de sesion');
    User.findOne({name:req.body.name, password:req.body.password},"",function(err,data){
        if(err){
           res.status(403).send({
                    error: true,
                    message: 'Error trying to signin'
                }); 
        }else{
            //if a user founded, then generate token
           if(data){
               //aqui se puede hacer una funcion anonima callback(err,token) para redireccion o hacer alguna accion
               var token = jwt.sign(data,config.phrase, {
                    expiresIn: 1440 // tiempo de expiración, checar documentacion
                });
               res.json({error:'false',message:'enjoy token', token:token})
           }else{
            res.json({error:'true', message:'User was not found' })    
           }
        }
    });
});

module.exports = router;