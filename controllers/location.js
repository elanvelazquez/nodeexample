//Packages
var express = require('express');
var router = express.Router();
var Location = require('../models/company').Location;

router.route("/")
    .get(function(req,res){
        console.log("List all Locations");
        Location.find(function(err, data){
            if(err){
                res.status(403).send({
                    error: true,
                    message: "Error trying to list all locations"
                });
            }else{
                res.json({
                    data: data,
                    error: false
                });
            }
            res.end();
        });
    })
    .post(function(req, res){
        console.log("Add Locations");
        //create a structure
        var data = {
            name: req.body.name,
            address: req.body.address
        };
        //create a location object
        var location = new Location(data);
        //save process
        location.save(function(err){
            if(err){
                res.status(403).send({
                    error: true
                    , message: 'Error adding user'
                });
            }else{
                res.end();
            }
            res.end();
        });
});


module.exports = router;