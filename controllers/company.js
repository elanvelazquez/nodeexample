//Packages
var express = require('express');
var router = express.Router();
var Company = require('../models/company').Company;
//Routes inside of /users
//Routes to add and get companies
router.route("/")
    .get(function (req, res) {
        console.log('/company');
        Company.find(function (err, data) {
            if (err) {
                res.status(403).send({
                    error: true,
                    message: 'Error users list'
                });
            } else {
                console.log(data);
                res.json({
                    data: data,
                    error: false
                });
            }
            res.end();
        });
    })
    .post(function (req, res) {
        console.log("post");
        var data = {
            name: req.body.name
        };
        var company = new Company(data);
        company.save(function (err) {
            if (err) {
                res.status(403).send({
                    error: true,
                    message: 'Error adding user'
                });
            } else {
                res.end();
            }
            res.end();
        });
    });
//Routes to edit and delete companies and list by id
router.route('/:id')
    .get(function (req, res) {
        //find by id with an id passed by url param
        Company.findById(req.params.id, function (err, data) {
            if (err) {
                res.status(403).send({
                    error: true,
                    message: 'Error trying to get company'
                });
            } else {
                console.log(data);
                res.json(data);
                res.end();
            }
            res.end();
        });
    })
    .put(function (req, res) {
        Company.findById(req.params.id, function (err, company) {
            //if not found a company by id
            if (err) {
                //send error
                res.status(403).send({
                    error: true,
                    message: 'Error trying to find the company.'
                });
            } else {
                //save company if the company exist
                company.save(function (err) {
                    if (err) {
                        //send error
                        res.status.(403).send({
                            error: 'true',
                            message: 'Error trying to update company.'
                        });
                    } else {
                        console.log("Company updated");
                    }
                });
                res.end();
            }
        });
    })
    .delete(function (req, res) {
        Company.findById(req.params.id, function (err, company) {
            if (err) {
                //handle error
                res.status(403).send({
                    error: true,
                    message: 'Error finding the company.'
                });
            } else {
                company.remove(function (err) {
                    if (err) {
                        //handle error
                        res.status(403).send({
                            error: true,
                            message: 'Error removing this company'
                        });
                    } else {
                        //handle success
                        console.log("Company removed");
                    }
                });
                res.end();
            }
        });

    });

//---------------end of routes----------------//
//Export router to controllers index.js
module.exports = router;
// eliminar parametros de json by request.