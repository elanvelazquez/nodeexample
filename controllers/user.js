//Packages
var express = require('express');
var router = express.Router();
var User = require('../models/user').User;
var config = require('../config');
//Route inside of /users
router.route('/')
    .get(function (req, res) {
        console.log('find all users');
        User.find(function(err,data){
            if(err){
                res.status(403).send({
                    error: true,
                    message: 'Error list user'
                });
            }else{
                res.json(data);
            }
        });
    })
    .post(function (req, res) {

    });

router.route('/:id')
    .get(function(req,res){
        User.findById(req.params.id,function(err,data){
             if (err) {
                            res.status(403).send({
                                error: true
                                , message: 'Error trying to get users'
                            });
                        } else {
                            console.log(data);
                            res.json(data);
                            res.end();
                        }
                    res.end();
        });
    })
    .put(function(req,res){
    
})
    .delete(function(req,res){
    
});


// ruta para la autenticación de usuarios
// mediante name y password

//------------end of routes-----------------//


//Export router to controllers index.js
module.exports = router;