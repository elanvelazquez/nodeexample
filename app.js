//packages
var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var app = express();
var config = require('./config');
var Promise = require('mpromise');
//--------End packages and imports --------------//
//Settings
var port = process.env.PORT || config.port;
//DB connection
mongoose.Promise = global.Promise;
mongoose.connect(config.database); // connect mongodb

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
//--------- End Settings-----------------//
//Set Middlewares and the private route
app.use('/api',require('./middlewares/'));
//---------End set Middlewares------------------//
//API's routes
app.use(require('./controllers'));
//app.use(require('./models'));
//----------End public API's routes---------------//
// Init server
//app.use('/',router);
app.listen(port);
console.log('Server on port:' + port);
//---------End Init Server --------------------//