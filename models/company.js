var mongoose = require("mongoose");
var Schema = mongoose.Schema;

//var config = require('../config');
//mongoose.connect(config.database);
/*
 private String name, description, horary, address, email, telephone, siteWeb, facebook, urlLogo, urlCompany, apiKey, appId;
    private List<LocalCommercial> listLocalCommercial;
    private List<Banner> banners;
    private List<NoticePush> avisos;
    private User user;
*/
var company_schema= new Schema({
    name:{type:"String",required: true},
    description:{type:"String"},
    horary:{type:"String"},
    address:{type:"String"},
    email:{type:"String"},
    telephone:{type:"String"},
    webSite:{type:"String"},
    facebook:{type:"String"},
    urlLogo:{type:"String"},
    urlCompany:{type:"String"},
    apiKey:{type:"String"},
    appId:{type:"String"},
    locations :[{type: Schema.Types.ObjectId, ref:'Location'}] 
});

//creating Company model on mongo
var Company = mongoose.model('Company',company_schema);
//Export the company model
module.exports.Company = Company;