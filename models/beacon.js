//Import packages
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var beacon_Schema = new  Schema({
    name:{type: "String", required:true},
    //geofences : [{type: Schema.Types.ObjectId, ref:'Geofence'}]
});

var Beacon = mongoose.model('Beacon',beacon_Schema);

module.exports.Beacon = Beacon;