// Import packages
var mongoose = require('mongoose');
var Shchema = mongoose.Schema;

var campaign_Schema = new Schema({
    name = {type: "String", required: true},
    beacons = [{type: Schema.Types.ObjectId, ref: 'Beacon' }]
});

var Campaign = mongoose.model('Campaign',campaign_Schema);

module.exports.Campaign = Campaign;