//Import packages
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var user_schema = new Schema({
    name : {type:"String", unique : true, required : true},
    password : {type:"String", required : true},
    role : {type: String ,enum :['ADMIN','USER']}
});

//Creating Schema
var User = mongoose.model("User",user_schema);
//Export User model
module.exports.User = User;