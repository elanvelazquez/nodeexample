//Import packages
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var location_Schema = new Schema({
    name:{type : "String", required : true},
    address:{type : "String"},
    user: {type: Schema.Types.ObjectId, ref:'User'}
});

//Creating a model
var Location = mongoose.model("Location",location_Schema);
//Import Location
module.exports.Location = Location;