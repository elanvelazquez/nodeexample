//Packages
var express = require('express');
var bodyParser = require('body-parser');
var router = express.Router();
var jwt = require('jsonwebtoken');
var app = express();
var config = require('../config');
// route middleware to verify a token
router.use(function(req, res, next) {

  // check header or url parameters or post parameters for token
  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  // decode token
  if (token) {
    // verifies secret and checks exp
    jwt.verify(token, config.phrase, function(err, decoded) {      
      if (err) {
        return res.json({ success: false, message: 'Failed to authenticate token.' });    
      } else {
        // if everything is good, save to request for use in other routes
        req.decoded = decoded;    
        next();
      }
    });

  } else {
    // if there is no token return a token
    return res.status(403).send({ 
        success: false, 
        message: 'No token provided.' 
    });
    
  }
});

module.exports= router;